FROM centos:7

# System
ENV LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_ALL=en_US.UTF-8
RUN yum update -y && yum install -y vim lsof net-tools java-1.8.0-openjdk.x86_64 && \
    yum clean headers && \
    yum clean packages && \
    yum clean metadata

# openoffice
ADD INIT/Apache_OpenOffice_4.1.7_Linux_x86-64_install-rpm_zh-CN.tar.gz /root/
RUN cd /root/zh-CN/RPMS/ && yum localinstall -y *.rpm &&  rm -rf /root/zh-CN/
COPY INIT/simsun.ttc /opt/openoffice4/share/fonts/truetype/
COPY INIT/simsun.ttf /opt/openoffice4/share/fonts/truetype/

# install
COPY INSTALL /root/INSTALL/
COPY *.sh /root/

EXPOSE 8100

WORKDIR /root/

ENTRYPOINT /root/docker.sh start
