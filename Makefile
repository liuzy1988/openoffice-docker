all:
	wget -c https://udomain.dl.sourceforge.net/project/openofficeorg.mirror/4.1.7/binaries/zh-CN/Apache_OpenOffice_4.1.7_Linux_x86-64_install-rpm_zh-CN.tar.gz -O INIT/Apache_OpenOffice_4.1.7_Linux_x86-64_install-rpm_zh-CN.tar.gz
	docker load -i centos7.tar || docker pull centos:7 # 从本地导入或远程下载centos7
	docker build -t openoffice . # 构建镜相
	docker run -itd --name openoffice -v `pwd`:/outside -p 8100:8100 openoffice /bin/bash
	docker exec -it openoffice /bin/bash
